from problog.program import PrologString
from problog import get_evaluatable
from ProblogParser import *
from GraphicalRepresentation import *
from Zone import Zone
from MovingObject import MovingObject

import time
import os
import sys

# Threshold for priority rules
free_zone_threshold = 0.95

# Maximum number of time steps.
max_time_steps = 20

# Make ProblogParser object
problog_parser = ProblogParser()

# File to write output
write_file = open('output.txt','a')

# Make GraphicalRepresentation object
graphical_representation = GraphicalRepresentation()

# Location of problog main model
main_model = 'problog' + os.sep + 'main_model.pl'

def create_zones(lanes, pedestrian_crossing_zones):
    """
    Create zone objects
    :param lanes: the lanes of the intersection
    :param pedestrian_crossing_zones: the zones wich contain a pedestrian crossing
    :return: zones
    """
    zones = {}

    zones_intersection = {
        'zl': Zone('zl', (0, 0), True, False),
        'zd': Zone('zd', (1, 0), True, False),
        'zo': Zone('zo', (0, 1), True, False),
        'zr': Zone('zr', (1, 1), True, False)
    }

    zones_lo = {
        'zlo1': Zone('zlo1', (-1, 1), False, False),
        'zlo2': Zone('zlo2', (-2, 1), False, False),
        'zplo1': Zone('zplo1', (-1, 2), False, True),
        'zplo2': Zone('zplo2', (-2, 2), False, True)
    }

    zones_li = {
        'zli1': Zone('zli1', (-1, 0), False, False),
        'zli2': Zone('zli2', (-2, 0), False, False),
        'zpli1': Zone('zpli1', (-1, -1), False, True),
        'zpli2': Zone('zpli2', (-2, -1), False, True)
    }

    zones_do = {
        'zdo1': Zone('zdo1', (0, -1), False, False),
        'zdo2': Zone('zdo2', (0, -2), False, False),
        'zpdo1': Zone('zpdo1', (-1, -1), False, True),
        'zpdo2': Zone('zpdo2', (-1, -2), False, True)
    }

    zones_di = {
        'zdi1': Zone('zdi1', (1, -1), False, False),
        'zdi2': Zone('zdi2', (1, -2), False, False),
        'zpdi1': Zone('zpdi1', (2, -1), False, True),
        'zpdi2': Zone('zpdi2', (2, -2), False, True)
    }

    zones_oo = {
        'zoo1': Zone('zoo1', (1, 2), False, False),
        'zoo2': Zone('zoo2', (1, 3), False, False),
        'zpoo1': Zone('zpoo1', (2, 2), False, True),
        'zpoo2': Zone('zpoo2', (2, 3), False, True)
    }

    zones_oi = {
        'zoi1': Zone('zoi1', (0, 2), False, False),
        'zoi2': Zone('zoi2', (0, 3), False, False),
        'zpoi1': Zone('zpoi1', (-1, 2), False, True),
        'zpoi2': Zone('zpoi2', (-1, 3), False, True)
    }

    zones_ro = {
        'zro1': Zone('zro1', (2, 0), False, False),
        'zro2': Zone('zro2', (3, 0), False, False),
        'zpro1': Zone('zpro1', (2, -1), False, True),
        'zpro2': Zone('zpro2', (3, -1), False, True)
    }

    zones_ri = {
        'zri1': Zone('zri1', (2, 1), False, False),
        'zri2': Zone('zri2', (3, 1), False, False),
        'zpri1': Zone('zpri1', (2, 2), False, True),
        'zpri2': Zone('zpri2', (3, 2), False, True)
    }

    zones_per_lane = {
        'lo': zones_lo,
        'ro': zones_ro,
        'oo': zones_oo,
        'do': zones_do,
        'li': zones_li,
        'ri': zones_ri,
        'oi': zones_oi,
        'di': zones_di
    }

    zones.update(zones_intersection)

    # Add zones from lanes
    for lane in lanes:
        zones.update(zones_per_lane[lane])

    # Update pedestrian crossing zones
    for pedestrian_crossing_zone in pedestrian_crossing_zones:
        zones[pedestrian_crossing_zone].pedestrian_crossing_zone = True

    return zones

def update_moving_object_positions(next_zone_move, time_step, moving_objects, timestep_zones_cars, timestep_zones_pedestrians, zones, graphic_representation):
    """
    Update the positions of the moving objects
    :param next_zone_move: the next zone of self
    :param time_step: the current time step
    :param moving_objects: all moving objects
    :param timestep_zones_cars: dict containing zones of cars per time step
    :param timestep_zones_pedestrians: dict containing zones of pedestrians per time step
    :param zones: zones dict
    :param graphic_representation: boolean defining whether graphic representation is done
    :return: time step, updated moving objects, boolean indicating if time step has cars, boolean indicating if time step has pedestrians
    """

    # Update position of self if it has new zone
    if next_zone_move is not None:
        time_step += 1
        print 'Time step: ' + str(time_step)
        self = moving_objects['self']
        self.zone = next_zone_move

    # Name of object
    objectname = 0
    # Boolean indicating whether time step contains pedestrians
    has_pedestrians = False
    # Boolean indicating whether time step contains cars
    has_cars = False

    # Update car positions
    if time_step in timestep_zones_cars:
        has_cars = True
        for (probability, car_zone) in timestep_zones_cars[time_step]:
            objectname += 1
            if objectname in moving_objects:
                object = moving_objects[objectname]
                object.zone = zones[car_zone]
                object.probability = probability
                object.type = 'car'
            else:
                moving_objects[objectname] = MovingObject(objectname, zones[car_zone], probability, 'car')

    # Update pedestrian positions
    if time_step in timestep_zones_pedestrians:
        has_pedestrians = True
        for (probability, pedestrian_zone) in timestep_zones_pedestrians[time_step]:
            objectname += 1
            if objectname in moving_objects:
                object = moving_objects[objectname]
                object.zone = zones[pedestrian_zone]
                object.probability = probability
                object.type = 'pedestrian'
            else:
                moving_objects[objectname] = MovingObject(objectname, zones[pedestrian_zone], probability, 'pedestrian')

    # Remove remaining moving objects
    keys = moving_objects.keys()
    for key in keys:
        if key == 'self':
            continue
        elif key > objectname:
            del moving_objects[key]

    # Show and save graphical representation
    if graphic_representation:
        plt, fig = graphical_representation.plot_moving_objects(moving_objects.values(), zones.values())
        fig.savefig('figures/timestep_' + str(time_step) + '.png')
        raw_input("Press Enter to continue...")
        plt.close(fig)

    return time_step, moving_objects, has_cars, has_pedestrians

def empty_timestep_model(empty_cars, empty_pedestrians):
    """
    Model extensions if no cars or pedestrians are present
    :param empty_cars: boolean indiciating whether there are no cars
    :param empty_pedestrians: boolean indiciating whether there are no pedestrians
    :return: model extensions
    """
    model_string = ''

    # When no cars, add start_lane fact and set evidence of cars at all zones to false
    if empty_cars:
        all_cars_fact = problog_parser.make_fact('all_cars', ['[]'])
        clause_string = 'zone(Zone)'
        not_evidence_cars = problog_parser.make_evidence_with_clause('zone_has_car', ['Zone'], False, clause_string)
        start_lane_string = 'start_lane(notDefined, notDefined).'
        model_string += all_cars_fact + '\n' + not_evidence_cars + '\n' + start_lane_string + '\n'

    # When no pedestrians, set evidence of pedestrians at all zones to false
    if empty_pedestrians:
        clause_string = 'zone(Zone)'
        not_evidence_pedestrians = problog_parser.make_evidence_with_clause('zone_has_pedestrian', ['Zone'], False, clause_string)
        model_string += not_evidence_pedestrians + '\n'

    return model_string


def main(simulation_file, intersection_lanes_file, graphic_representation):
    """
    Main program
    :param simulation_file: the simulation filename
    :param intersection_lanes_file: the lanes of the intersection filename
    :param graphic_representation: boolean defining whether graphic representation is done
    """
    with open(main_model, 'r') as main_file:
        standard_model=main_file.read()

    # Retrieve lanes and model string of lanes
    intersection_lane_model, lanes = problog_parser.parse_intersection_file(intersection_lanes_file)

    # Parse simulations file
    general_evidences, timestep_evidences, timestep_zones_cars, timestep_zones_pedestrians, pedestrian_crossing_zones, route_self = problog_parser.parse_file(simulation_file)

    # Create the zones based on the existing lanes.
    zones = create_zones(lanes, pedestrian_crossing_zones)

    # Construct basis of model
    start_model = standard_model + intersection_lane_model + general_evidences

    # Add self to moving_objects list
    self_start_position = route_self[0]
    moving_objects = {'self': MovingObject('self', zones[self_start_position],1, 'car')}

    # Convert zone names to zone objects in route of self
    temp_route_self = []
    for zone_string in route_self:
        temp_route_self.append(zones[zone_string])
    route_self = temp_route_self

    # Next zone self may move to.
    next_zone_move = None

    # Zone number to check
    zone_number = 1

    original_zone_number = 1

    # Current time step
    time_step = 0

    # Run until maximum time steps is reached or when self reached its destination
    while time_step < max_time_steps and len(route_self) != zone_number:
        # Update moving object positions
        time_step, moving_objects, has_cars, has_pedestrians = update_moving_object_positions(next_zone_move, time_step, moving_objects, timestep_zones_cars, timestep_zones_pedestrians, zones, graphic_representation)

        # Boolean indicating wheter zone check is done
        zone_check_done = False

        next_zone_move = None

        # Add time step specific information to the model
        timestep_model = timestep_evidences.get(time_step,'')
        model = start_model + timestep_model

        # Update original zone number
        if original_zone_number != zone_number:
            original_zone_number += 1

        # Currently checked zone number
        zone_number = original_zone_number

        # The zone where the car is currently positioned at
        current_car_zone = route_self[original_zone_number-1]

        while not zone_check_done:
            self_has_priority = True
            # Zone to check
            zone = route_self[zone_number]
            # Moving objects that do not have priority on self. Not used at this time, may be used in extensions
            moving_objects_give_priority = []
            # Moving objects that have priority on self. Not used at this time, may be used in extensions
            moving_objects_have_priority = []

            free_zone_query = ''

            # Zone name of currnent position of self
            self_zone_name = current_car_zone.name

            # Add position of self to model
            model += problog_parser.make_evidence('location', ['self', self_zone_name], True) + '\n'
            model += problog_parser.make_evidence_with_clause('location', ['self', 'Z'], False, 'zone(Z),Z\=' + self_zone_name) + '\n'

            # Situation when other moving objects are available
            if len(moving_objects.values()) != 1:
                if not has_cars:
                    model += empty_timestep_model(True, False) + '\n'
                if not has_pedestrians:
                    model += empty_timestep_model(False, True) + '\n'

                # Query for each moving object if zone is free
                for moving_object in moving_objects.values():
                    if moving_object.name != 'self':
                        moving_object_zone = moving_object.zone.name
                        free_zone_query += problog_parser.make_query('free_zone', ['self', moving_object_zone, 'intersection', zone.name]) + '\n'

                new_model = model + free_zone_query

                # Start query timing
                start = time.time()

                # Execute query
                p = PrologString(new_model)
                priority_query = get_evaluatable().create_from(p, propagate_evidence = True).evaluate()
                query_time = time.time() - start

                # Print and write query results
                write_file.write(str('Time step: ' + str(time_step) + '\n' + 'Time query: ' + str(query_time) + '\n'))
                for query, value in priority_query.iteritems():
                    query_string = str(query) + ': ' + str(round(value, 3))
                    write_file.write(str(query_string + '\n'))
                    print query_string

                # Decide wheter zone is free compared to each moving objects. The objects are saved in lists. This is not further used at this time, may be used in extensions
                for query, chance in priority_query.iteritems():
                    moving_object = problog_parser.get_query_parameters(str(query))[1]
                    if chance > free_zone_threshold:
                        moving_objects_give_priority.append(moving_object)
                    else:
                        moving_objects_have_priority.append(moving_object)
                        self_has_priority = False

            # Situation where self is only moving object
            else:
                # Construct special query without moving objects
                no_free_zone_query = problog_parser.make_query('no_free_zone', ['self', 'intersection', zone.name])
                new_model = model + '\n' + empty_timestep_model(True, True) + '\n' + no_free_zone_query

                # Start query timing
                start = time.time()

                # Execute query
                p = PrologString(new_model)
                priority_query = get_evaluatable().create_from(p, propagate_evidence=True).evaluate()
                query_time = time.time() - start

                # Print and write query results
                write_file.write(str('Time step: ' + str(time_step) + '\n' + 'Time query: ' + str(query_time) + '\n'))
                for query, value in priority_query.iteritems():
                    query_string = str(query) + ': ' + str(round(value, 3))
                    write_file.write(str(query_string + '\n'))
                    print query_string

                # Decide wheter zone is free
                for query, chance in priority_query.iteritems():
                    free_zone_chance = 1.0 - chance
                    if not free_zone_chance > free_zone_threshold:
                        self_has_priority = False

            # Situation that self has priority on all moving objects for the zone
            if self_has_priority:
                # If the next zone is not at the intersection or if the car is already at the intersection, move to the next zone
                if (not zone.crossing and not zone.pedestrian_crossing_zone) or current_car_zone.crossing or current_car_zone.pedestrian_crossing_zone:
                    if next_zone_move == None:
                        next_zone_move = zone
                    zone_check_done = True
                # Continue with checking next zone if car is not already at the intersection, check whole route at intersection before entering it
                else:
                    if next_zone_move == None:
                        next_zone_move = zone
                zone_number += 1

            # If self needs to give priority on at least one moving object, go one time step further and redo all zone checks.
            else:
                next_zone_move = None
                zone_number = original_zone_number
                time_step += 1
                print 'Time step: ' + str(time_step)
                zone_check_done = True

        if original_zone_number != zone_number:
            original_zone_number += 1
        zone_number = original_zone_number

    # Update moving object positions
    update_moving_object_positions(next_zone_move, time_step, moving_objects, timestep_zones_cars, timestep_zones_pedestrians, zones, graphic_representation)

if __name__ == "__main__":
    # # Simulation file name
    # simulation_file = 'Simulations_text_2.txt'
    # # Intersection lanes file name
    # intersection_lanes_file = 'intersection_layout.txt'
    # # Save (and show) graphical representation
    # graphic_representation = True

    # Simulation file name
    simulation_file = sys.argv[1]
    # Intersection lanes file name
    intersection_lanes_file = sys.argv[2]
    # Save (and show) graphical representation
    graphic_representation = sys.argv[3]
    if graphic_representation == '--graphics':
        graphic_representation = True
    elif graphic_representation == '--no-graphics':
        graphic_representation = False
    else:
        raise Exception('Wrong graphics option')

    main(simulation_file, intersection_lanes_file, graphic_representation)


