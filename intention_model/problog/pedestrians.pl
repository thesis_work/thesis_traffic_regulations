%%% Pedestrian %%%
% Define a pedestrian.
pedestrian(PedestrianZone) :- zone_has_pedestrian(PedestrianZone).

% If the pedestrian is located at the zone, it uses the pedestrian zone.
intention_taking_pedestrian_zone(Pedestrian, Zone) :-
        Pedestrian = Zone.

% If the pedestrian is at the sidewalk next to the pedestrian crossing and it is oriented towards pedestrian crossing, it will use the pedestrian zone.
intention_taking_pedestrian_zone(Pedestrian, Zone) :-
        sidewalk_zone(Zone, SidewalkZone),
        Pedestrian = SidewalkZone,
        lane_of_zone(Zone, Lane),
        orientation_pedestrian(Pedestrian, OrientationPedestrian),
        oriented_towards_lane(Lane, OrientationPedestrian).

% If the pedestrian is located at the pedestrian crossing located in the opposite direcion of the lane and is oriented towards the Zone, it will use the pedestriaon zone.
intention_taking_pedestrian_zone(Pedestrian, Zone) :-
        next_pedestrian_zone(Zone, NextZone),
        pedestrian_crossing_zone(NextZone),
        Pedestrian = NextZone,
        lane_of_zone(NextZone, Lane),
        orientation_pedestrian(Pedestrian, OrientationPedestrian),
        oriented_towards_lane(Lane, OrientationPedestrian).

% If the pedestrian is located at the sidewalk at the opposite direction of the lane and it is oriented towards the lane, it will use the pedestrian zone.
intention_taking_pedestrian_zone(Pedestrian, Zone) :-
        next_pedestrian_zone(Zone, NextZone),
        pedestrian_crossing_zone(NextZone),
        sidewalk_zone(NextZone, SidewalkZone),
        Pedestrian = SidewalkZone,
        lane_of_zone(NextZone, Lane),
        orientation_pedestrian(Pedestrian, OrientationPedestrian),
        oriented_towards_lane(Lane, OrientationPedestrian).

% Orientation for pedestrian if oriented towards Lane.
oriented_towards_lane(Lane, Orientation) :-
        (Lane = di, Orientation = l);
        (Lane = do, Orientation = r);
        (Lane = li, Orientation = o);
        (Lane = lo, Orientation = d);
        (Lane = oi, Orientation = r);
        (Lane = oo, Orientation = l);
        (Lane = ri, Orientation = d);
        (Lane = ro, Orientation = o).
