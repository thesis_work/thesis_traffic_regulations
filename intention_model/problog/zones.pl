% Get the lane where the zone is part of.
lane_of_zone(Zone, Lane) :-
        lane_zones(Lane, Zones),
        member(Zone, Zones).

% Check whether zone is intersection zone.
intersection_zone(Zone) :-
        zone(Zone),
        intersection_zones(Zones),
        member(Zone, Zones).

% Check whether the Zone is in the route of the Car to the Destination.
zone_in_route(Zone, Car, StartLane, Destination) :-
        zones_to_follow(Car, StartLane, Destination, ZonesToFollow),
        member(Zone,ZonesToFollow).

% The zone is in the route when the car is standing at it.
zone_in_route(Zone, Car, StartLane, Destination) :-
        Zone = Car.

%% The zones that need to be followed when the car is positioned at Zone with StartLane and Destination.
% The Zone is at the intersection.
zones_to_follow(Zone, StartLane, Destination, ZonesToFollow) :-
        intersection_zone(Zone),
        zones_followed(StartLane, Destination, Zones_list),
        member(Zone, Zones_list),
        remaining_zones(Zone, Zones_list, ZonesToFollow).

% The Zone is at the incoming StartLane.
zones_to_follow(Zone, StartLane, Destination, ZonesToFollow) :-
        lane_of_zone(Zone, StartLane),
        zones_followed(StartLane, Destination, ZonesToFollow).

% The Zone is at an outgoing lane.
zones_to_follow(Zone, StartLane, Destination, ZonesToFollow) :-
        lane_of_zone(Zone, LaneZone),
        outgoing_lane(LaneZone),
        ZonesToFollow = [].

% Get remaning zones starting from current zone.
remaining_zones(CurrentZone, [CurrentZone|Rest], Rest).

remaining_zones(CurrentZone, [FirstZone|Rest], ZonesToFollow) :-
        CurrentZone \= FirstZone,
        remaining_zones(CurrentZone, Rest, ZonesToFollow).
