from problog.extern import problog_export

# Return the orientation of the lane, based on the naming convention (first letter of lane is the orientation).
@problog_export('+str', '-str')
def lane_road(lane):
    return lane[0]

# Return the outgoing lane corresponding the road.
@problog_export('+str', '-str')
def lane_out(road):
    return road + 'o'

# Return the incoming lane corresponding the road.
@problog_export('+str', '-str')
def lane_in(road):
    return road + 'i'

# Return the zone next to the pedestrian zone (the sidewalk zone).
@problog_export('+str', '-str')
def sidewalk_zone(pedestrian_zone):
    return 'zp' + pedestrian_zone[1:]

# Get the zone next to the given zone in the opposite direction of the lane, used for pedestrians.
@problog_export('+str', '-str')
def next_pedestrian_zone(zone):
    if 'o' in zone:
        zone = zone.replace('o', 'i')
    elif 'i' in zone:
        zone = zone.replace('i', 'o')

    return zone

# Return the first part of the intermediate orientation
@problog_export('+str', '-str')
def first_intermediate_orientation(intermediate_orientation):
    return intermediate_orientation[0]

# Return the second part of the intermediate orientation
@problog_export('+str', '-str')
def second_intermediate_orientation(intermediate_orientation):
    return intermediate_orientation[1]
