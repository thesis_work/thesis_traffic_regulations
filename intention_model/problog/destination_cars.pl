%%% Destination cars %%%
% If no indicator information is available, chances are equal that car goes to left/straight/right.
NP1 :: destination(Car, D1); NP2 :: destination(Car, D2); NP3 :: destination(Car, D3) :-
        \+ indicator_information(Car),
        start_lane(Car, Lane),
        incoming_lane(Lane),
        lane_road(Lane, Road),
        left_out(Road, D1),
        chance_destination(Car, D1, 1/3, 0.01, P1),
        straight_out(Road, D2),
        chance_destination(Car, D2, 1/3, 0.01, P2),
        right_out(Road, D3),
        chance_destination(Car, D3, 1/3, 0.01, P3),
        normalize_destination(P1, P2, P3, NP1, NP2, NP3).

% If the left indicator from the Car is on, the chance is high that the Car will go to the left.
NP1 :: destination(Car, D1); NP2 :: destination(Car, D2); NP3 :: destination(Car, D3) :-
        indicator_left(Car,on),
        start_lane(Car, Lane),
        incoming_lane(Lane),
        lane_road(Lane, Road),
        left_out(Road, D1),
        chance_destination(Car, D1, 0.95, 0.01, P1),
        straight_out(Road, D2),
        chance_destination(Car, D2, 0.025, 0.005, P2),
        right_out(Road, D3),
        chance_destination(Car, D3, 0.025, 0.005, P3),
        normalize_destination(P1, P2, P3, NP1, NP2, NP3).

% If the left indicator from the Car is off, the chance is low that the Car will go to the left.
NP1 :: destination(Car, D1); NP2 :: destination(Car, D2); NP3 :: destination(Car, D3) :-
        start_lane(Car, Lane),
        incoming_lane(Lane),
        lane_road(Lane, Road),
        indicator_left(Car,off),
        left_out(Road, D1),
        chance_destination(Car, D1, 0.05, 0.005, P1),
        straight_out(Road, D2),
        chance_destination(Car, D2, 0.475, 0.01, P2),
        right_out(Road, D3),
        chance_destination(Car, D3, 0.475, 0.01, P3),
        normalize_destination(P1, P2, P3, NP1, NP2, NP3).


% If the right indicator from the Car is on, the chance is high that the Car will go to the right.
NP1 :: destination(Car, D1); NP2 :: destination(Car, D2); NP3 :: destination(Car, D3) :-
        indicator_right(Car,on),
        start_lane(Car, Lane),
        incoming_lane(Lane),
        lane_road(Lane, Road),
        left_out(Road, D1),
        chance_destination(Car, D1, 0.025, 0.005, P1),
        straight_out(Road, D2),
        chance_destination(Car, D2, 0.025, 0.005, P2),
        right_out(Road, D3),
        chance_destination(Car, D3, 0.95, 0.01, P3),
        normalize_destination(P1, P2, P3, NP1, NP2, NP3).

% If the right indicator from the Car is off, the chance is low that the Car will go to the right.
NP1 :: destination(Car, D1); NP2 :: destination(Car, D2); NP3 :: destination(Car, D3) :-
        start_lane(Car, Lane),
        incoming_lane(Lane),
        lane_road(Lane, Road),
        indicator_right(Car,off),
        left_out(Road, D1),
        chance_destination(Car, D1, 0.475, 0.01, P1),
        straight_out(Road, D2),
        chance_destination(Car, D2, 0.475, 0.01, P2),
        right_out(Road, D3),
        chance_destination(Car, D3, 0.05, 0.005, P3),
        normalize_destination(P1, P2, P3, NP1, NP2, NP3).

% Normalize chances
normalize_destination(P1, P2, P3, NP1, NP2, NP3) :-
        TotalSum is P1 + P2 + P3,
        NP1 is P1/TotalSum,
        NP2 is P2/TotalSum,
        NP3 is P3/TotalSum.

% Add constraint to normalize chances for all destinations together.
% For each Car, at most one destination is true.
atmost_one(Car) :-
        destination(Car,D1),
        outgoing_lane(D1),
	       \+ (destination(Car, D2), D1 \= D2).

evidence(atmost_one(Car)) :-
        all_cars(Cars),
        member(Car, Cars).

% If Destination is defined and not forbidden and the Car is positioned at the route for the destination, the chance P is equal to the P1.
chance_destination(Car, Destination, P1, P2, P) :-
        Destination \= notDefined,
        \+ forbidden_direction_lane(_,Destination),
        reach_destination(Car, Destination),
        P = P1.

% If Destination is forbidden, the chance P is equal to P2.
chance_destination(Car, Destination, P1, P2, P) :-
        forbidden_direction_lane(_,Destination),
        P = P2.

% If Destination is not defined, the chance P is equal to 0.
chance_destination(Car, Destination, P1, P2, P) :-
        Destination == notDefined,
        P = 0.

% If the Car cannot reach the destination, the chance P is equal to 0.
chance_destination(Car, Destination, P1, P2, P) :-
        \+ reach_destination(Car, Destination),
        P = 0.

% Determine whether a car can reach a destination. This is the case if it is positioned at the StartPoint or Destination of the route, or if it is positioned at a zone of the route.
reach_destination(Car, Destination) :-
        start_lane(Car, StartLane),
        lane_of_zone(Car, StartLane).

reach_destination(Car, Destination) :-
        lane_of_zone(Car, Destination).

reach_destination(Car, Destination) :-
        start_lane(Car, StartLane),
        zones_followed(StartLane, Destination, Zones),
        member(Car, Zones).
