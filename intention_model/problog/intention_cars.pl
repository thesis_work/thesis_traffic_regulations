%%% Intention cars %%%
% If a car is stationary and is not positioned at the zone, it will give priority.
intention_give_priority(Car, Zone) :-
				stationary_car(Car),
				Car \= Zone.

% If a car is moving, it is checked if the car is able to stop before the zone and whether it is stopping.
intention_give_priority(Car, Zone) :-
				\+ stationary_car(Car),
				able_to_stop_before_zone(Car, Zone),
				is_stopping(Car, Zone).
