"""
Class representing pedestrians and cars.
"""

class MovingObject:

    def __init__(self, name, zone, probability, type):
        # The name of the moving object
        self.name = name

        # The zone of the moving object
        self.zone = zone

        # The probability of the moving object
        self.probability = probability

        #The type of the moving object.
        self.type = type

