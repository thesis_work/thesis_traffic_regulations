"""
Class constructing graphical representation
"""

import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np

class GraphicalRepresentation:

    def add_rectangle(self, ax, xPosition, yPosition, width, height, color, filled, zorder, transparency):
        ax.add_patch(
            patches.Rectangle(
                (xPosition, yPosition),
                width,
                height,
                facecolor=color,
                fill=filled,
                edgecolor = 'k',
                zorder = zorder,
                alpha = transparency
            )
        )

    def add_text(self, ax, text, xPosition, yPosition, zorder):
        ax.annotate(text, (xPosition, yPosition), color='w',
                     fontsize=8, ha='center', va='center', zorder = zorder)

    def add_zone(self, ax, xPosition, yPosition, name, type):
        width = 1
        height = 1

        # Change center coordinate to left coordinate of rectangle
        rectXPosition = xPosition - width/2.0
        rectYPosition = yPosition - height/2.0

        if type == 'intersection':
            color = 'red'
        elif type == 'lane':
            color = 'blue'
        self.add_rectangle(ax, rectXPosition, rectYPosition, width, height, color, True, 0, 1)
        self.add_text(ax, name, xPosition, yPosition, 0)

    def add_sidewalk_zone(self, ax, xPosition, yPosition):
        width = 1
        height = 1

        # Change center coordinate to left coordinate of rectangle
        rectXPosition = xPosition - width / 2.0
        rectYPosition = yPosition - height / 2.0

        color = 'grey'
        self.add_rectangle(ax, rectXPosition, rectYPosition, width, height, color, True, 0, 1)


    def add_car(self, ax, xPosition, yPosition, name, probability):
        width = 0.7
        height = 0.7

        # Change center coordinate to left coordinate of rectangle
        rectXPosition = xPosition - width/2.0
        rectYPosition = yPosition - height/2.0

        if name == 'self':
            color = 'green'
        else:
            color = '#2299DD'

        self.add_rectangle(ax, rectXPosition, rectYPosition, width, height, color, True, 2, probability)
        self.add_text(ax, name, xPosition, yPosition, 2)


    def add_pedestrian(self, ax, xPosition, yPosition, name, probability):
        width = 0.4
        height = 0.4

        # Change center coordinate to left coordinate of rectangle
        rectXPosition = xPosition - width / 2.0
        rectYPosition = yPosition - height / 2.0

        color = 'yellow'

        self.add_rectangle(ax, rectXPosition, rectYPosition, width, height, color, True, 2, probability)
        self.add_text(ax, name, xPosition, yPosition, 2)

    def add_pedestrian_crossing(self, ax, xPosition, yPosition, road):
        width = 1
        height = 1
        if road == 'd' or road == 'o':
            rectYPosition = yPosition - height / 2.0
            rectXPosition1 = xPosition - width/2.0
            rectXPosition2 = xPosition + width/5.0 - width/2.0
            rectXPosition3 = xPosition + 2*width/5.0 - width/2.0
            rectXPosition4 = xPosition + 3*width/5.0 - width/2.0
            rectXPosition5 = xPosition + 4*width/5.0 - width/2.0
            self.add_rectangle(ax, rectXPosition1, rectYPosition, width/5.0, height, 'white', True, 1, 1)
            self.add_rectangle(ax, rectXPosition2, rectYPosition, width/5.0, height, 'black', True, 1, 1)
            self.add_rectangle(ax, rectXPosition3, rectYPosition, width/5.0, height, 'white', True, 1, 1)
            self.add_rectangle(ax, rectXPosition4, rectYPosition, width/5.0, height, 'black', True, 1, 1)
            self.add_rectangle(ax, rectXPosition5, rectYPosition, width/5.0, height, 'white', True, 1, 1)

        elif road == 'l' or road == 'r':
            rectXPosition = xPosition - width / 2.0
            rectYPosition1 = yPosition - height / 2.0
            rectYPosition2 = yPosition + height / 5.0 - height / 2.0
            rectYPosition3 = yPosition + 2 * height / 5.0 - height / 2.0
            rectYPosition4 = yPosition + 3 * height / 5.0 - height / 2.0
            rectYPosition5 = yPosition + 4 * height / 5.0 - height / 2.0
            self.add_rectangle(ax, rectXPosition, rectYPosition1, width, height / 5.0, 'white', True, 1, 1)
            self.add_rectangle(ax, rectXPosition, rectYPosition2, width, height / 5.0, 'black', True, 1, 1)
            self.add_rectangle(ax, rectXPosition, rectYPosition3, width, height / 5.0, 'white', True, 1, 1)
            self.add_rectangle(ax, rectXPosition, rectYPosition4, width, height / 5.0, 'black', True, 1, 1)
            self.add_rectangle(ax, rectXPosition, rectYPosition5, width, height / 5.0, 'white', True, 1, 1)

    def plot_intersection(self, zones):
        fig = plt.figure()
        ax1 = fig.add_subplot(111, aspect='equal')
        plt.yticks(np.arange(-3, 5, 1))
        plt.xticks(np.arange(-3, 5, 1))
        for zone in zones:
            (xpos, ypos) = zone.position
            if zone.sidewalk:
                self.add_sidewalk_zone(ax1, xpos, ypos)
            elif zone.pedestrian_crossing_zone:
                road = zone.name[1]
                self.add_pedestrian_crossing(ax1, xpos, ypos, road)
            else:
                name = zone.name
                crossing = zone.crossing
                if crossing:
                    type = 'intersection'
                else:
                    type = 'lane'
                self.add_zone(ax1, xpos, ypos, name, type)

        return fig

    def plot_moving_objects(self, movingobjects, zones):
        fig = self.plot_intersection(zones)
        ax = fig.add_subplot(111, aspect='equal')
        for object in movingobjects:
            (xpos, ypos) = object.zone.position
            name = object.name
            probability = object.probability
            type = object.type
            if type == 'car':
                self.add_car(ax, xpos, ypos, name, probability)
            elif type == 'pedestrian':
                self.add_pedestrian(ax, xpos, ypos, name, probability)
        plt.yticks([])
        plt.xticks([])
        plt.show(block=False)
        return plt, fig


