"""
Class parsing the simulations file
"""
class ProblogParser:

    def parse_query_answer(self, query):
        """
        Parse the answer of a query
        :param query: the answer of the query to parse
        :return: name and parameters of the answer of the query
        """
        split_name_parameters = query.split('(', 1)
        name_query = split_name_parameters[0]
        parameters_string = split_name_parameters[1].replace(')', '')
        parameters = parameters_string.split(',')

        return (name_query, parameters)

    def get_query_parameters(self, query):
        """
        Get parameters of a query
        :param query: query
        :return: parameters of the query
        """
        split_name_parameters = query.split('(', 1)
        parameters_string = split_name_parameters[1].replace(')', '')
        parameters = parameters_string.split(',')
        indices_remove = []
        add_params = []
        # Take care of list parameters, convert it to python list
        for index1 in range(1,len(parameters)):
            param = parameters[index1]
            if param.startswith('['):
                param_list = [param.replace('[','')]
                indices_remove.append(index1)
                for index2 in range(index1+1,len(parameters)):
                    part_list = parameters[index2].replace(' ','')
                    indices_remove.append(index2)
                    if part_list.endswith(']'):
                        param_list.append(part_list.replace(']',''))
                        add_params.append(param_list)
                    else:
                        param_list.append(part_list)
        if len(indices_remove) != 0:
            for index in sorted(indices_remove, reverse=True):
                parameters.pop(index)
            parameters += add_params

        return parameters

    def make_query(self, name, parameters):
        """
        Make a query
        :param name: name of the query
        :param parameters: parameters of the query
        :return: query
        """
        query_string = 'query(' + name + '('
        number_parameters = len(parameters)
        parameter_number = 0
        for param in parameters:
            parameter_number += 1
            query_string += param
            if parameter_number != number_parameters:
                query_string += ', '
        query_string += ')).'

        return query_string

    def make_evidence(self, name, parameters, evidence):
        """
        Make an evidence string
        :param name: name of the evidence
        :param parameters: parameters of the evidence
        :param evidence: boolean indicating if it is false or true evidence
        :return: evidence string
        """
        if evidence:
            evidence_string = 'evidence(' + name + '('
        else:
            evidence_string = 'evidence(not ' + name + '('
        number_parameters = len(parameters)
        parameter_number = 0
        for param in parameters:
            parameter_number += 1
            evidence_string += param
            if parameter_number != number_parameters:
                evidence_string += ', '
        evidence_string += ')).'
        return evidence_string

    def make_evidence_with_clause(self, name, parameters, evidence, clause):
        """
        Make an evidence string
        :param name: name of the evidence
        :param parameters: parameters of the evidence
        :param evidence: boolean indicating if it is false or true evidence
        :param clause: clause of the evidence
        :return: evidence string
        """
        if evidence:
            evidence_string = 'evidence(' + name + '('
        else:
            evidence_string = 'evidence(not ' + name + '('
        number_parameters = len(parameters)
        parameter_number = 0
        for param in parameters:
            parameter_number += 1
            evidence_string += param
            if parameter_number != number_parameters:
                evidence_string += ', '
        evidence_string += ')) :- '
        evidence_string += clause + '.'

        return evidence_string

    def make_probability(self, probability, name, parameters):
        """
        Make a probability string
        :param probability: probability of the fact
        :param name: name of the fact
        :param parameters: parameters of the fact
        :return: probability fact string
        """
        prob_string = str(probability) + ' :: ' + name + '('
        overwrite_string = 'overwrite(' + name + '('
        number_parameters = len(parameters)
        parameter_number = 0
        for param in parameters:
            parameter_number += 1
            prob_string += param
            overwrite_string += param
            if parameter_number != number_parameters:
                prob_string += ', '
                overwrite_string += ', '
        prob_string += ').'
        overwrite_string += ')).'

        result_string = prob_string + '\n' + overwrite_string

        return result_string

    def make_fact(self, name, parameters):
        """
        Make a fact without probability
        :param name: name of the fact
        :param parameters: parameters of the fact
        :return: fact string
        """
        result_string = name + '('
        number_parameters = len(parameters)
        parameter_number = 0
        for param in parameters:
            parameter_number += 1
            result_string += str(param)
            if parameter_number != number_parameters:
                result_string += ', '
        result_string += ').'

        return result_string

    def make_overwrite_fact(self, name, parameters):
        """
        Make overwrite fact
        :param name: name of the fact
        :param parameters: parameters of the fact
        :return: overwrite string
        """
        result_string = 'overwrite(' + name + '('
        number_parameters = len(parameters)
        parameter_number = 0
        for param in parameters:
            parameter_number += 1
            result_string += param
            if parameter_number != number_parameters:
                result_string += ', '
        result_string += ')).'

        return result_string

    def turn_params_in_list(self, parameters):
        """
        Turn parameters into a list
        :param parameters: parameters
        :return: parameters as a list
        """
        param_list = []
        splitted_parameters = parameters.split(',')
        for param in splitted_parameters:
            param_list.append(param)
        return param_list

    def parse_intersection_file(self, file_name):
        """
        Parse intersection file
        :param file_name: file name
        :return: model string and lanes
        """
        model = ''
        # Lanes the intersection consists of
        lanes = []

        with open(file_name, 'r') as int_layout:
            for line in int_layout:
                if line.startswith('%'):
                    continue
                else:
                    name, parameters = line.rstrip().split(' ')
                    params = self.turn_params_in_list(parameters)
                    model += self.make_fact(name, [params]) + '\n'
                    lanes.extend(params)

        return model, lanes

    def parse_file(self, file_name):
        """
        Parse simulation file
        :param file_name: file name
        :return: general evidences, evidence per time step, dict with cars per time step, dict with pedestrians per time step, pedestrian crossing zones and route of self
        """

        # Evidence that holds for each time step
        general_evidences = ''
        self_route = False
        general_information = False
        discrete_information = False

        time_step = 0

        # Dict with time step as key and as value the problog code that needs to be added to the model.
        timestep_evidences = {}
        # Dict with time step as key and as value pairs of (probability, zone) which is the probability that zone has a car.
        timestep_zones_cars = {}

        # Dict with time step as key and as value pairs of (probability, zone) which is the probability that zone has a pedestrian.
        timestep_zones_pedestrians = {}

        pedestrian_crossing_zones = []

        with open(file_name, 'r') as simulations:
            for line in simulations:
                # Building blocks of file
                if line.startswith('----BeginSimulation'):
                    continue
                elif line.startswith('%'):
                    continue
                elif line.startswith('--Self route'):
                    self_route = True
                    continue
                elif line.startswith('---GeneralInformation'):
                    self_route = False
                    general_information = True
                    continue
                elif line.startswith('---DiscreteInformation'):
                    general_information = False
                    discrete_information = True
                    continue
                elif line.startswith('**t='):
                    time_step = int(line[4:])
                    continue
                elif line.startswith('---EndSimulation'):
                    discrete_information = False
                    continue


                # Construct ProbLog code
                else:
                    # Information of Self
                    if self_route:
                        # Add destination evidence of Self
                        destination, route = line.rstrip().split(' ')
                        general_evidences += self.make_evidence('destination', ['self', destination], True) + '\n'

                        # Get route information of self
                        route_self = []
                        for zone in route.split(','):
                            route_self.append(zone)

                    # General information, evidences that are used for every time step
                    elif general_information:
                        # True evidence
                        if line.startswith('evidence'):
                            evidence = True
                            _, name, parameters = line.rstrip().split(' ')
                            params = self.turn_params_in_list(parameters)
                            evidence_string = self.make_evidence(name, params, evidence)
                            if name == 'pedestrian_crossing_zone':
                                pedestrian_crossing_zones.append(parameters)

                        # False evidence
                        elif line.startswith('not_evidence'):
                            evidence = False
                            _, name, parameters = line.rstrip().split(' ')
                            params = self.turn_params_in_list(parameters)
                            evidence_string = self.make_evidence(name, params, evidence)

                        # Probability fact
                        elif line.startswith('prob'):
                            _, probability, name, parameters = line.rstrip().split(' ')
                            params = self.turn_params_in_list(parameters)
                            evidence_string = self.make_probability(probability, name, params)
                            if name == 'pedestrian_crossing_zone':
                                pedestrian_crossing_zones.append(parameters)

                        # Evidence with clause
                        elif line.startswith('clause'):
                            evidence, name, parameters, clause = line.rstrip().split(' ')
                            params = self.turn_params_in_list(parameters)
                            if evidence.startswith('clause_evidence'):
                                evidence = True
                            else:
                                evidence = False
                            evidence_string = self.make_evidence_with_clause(name, params, evidence, clause)

                        # Overwrite fact
                        elif line.startswith('overwrite'):
                            _, name, parameters = line.rstrip().split(' ')
                            params = self.turn_params_in_list(parameters)
                            evidence_string = self.make_overwrite_fact(name, params)

                        # Annotated disjunction
                        elif line.startswith('composition_prob'):
                            _, literal_string = line.rstrip().split(' ')
                            evidence_string = literal_string

                        else:
                            raise Exception('Error in simulations file')


                        general_evidences += evidence_string + '\n'

                    # Information specific to a time step
                    elif discrete_information:
                        # Evidence with clause
                        if line.startswith('clause'):
                            evidence, name, parameters, clause = line.rstrip().split(' ')
                            params = self.turn_params_in_list(parameters)
                            if evidence.startswith('clause_evidence'):
                                evidence = True
                            else:
                                evidence = False
                            timestep_string = self.make_evidence_with_clause(name, params, evidence, clause)

                        # Probability fact
                        elif line.startswith('prob'):
                            _, probability, name, parameters = line.rstrip().split(' ')
                            params = self.turn_params_in_list(parameters)
                            timestep_string = self.make_probability(probability, name, params)
                            if name == 'zone_has_car':
                                timestep_zones_cars.setdefault(time_step, []).append((float(probability), parameters))
                            if  name == 'zone_has_pedestrian':
                                timestep_zones_pedestrians.setdefault(time_step, []).append((float(probability), parameters))

                        # Overwrite fact
                        elif line.startswith('overwrite'):
                            _, name, parameters = line.rstrip().split(' ')
                            params = self.turn_params_in_list(parameters)
                            timestep_string = self.make_overwrite_fact(name, params)

                        # Start lane car
                        elif line.startswith('start'):
                            _, car, startlane = line.rstrip().split(' ')
                            timestep_string = self.make_fact('start_lane', [car, startlane])

                        # Annotated disjunction
                        elif line.startswith('composition_prob'):
                            _, literal_string = line.rstrip().split(' ')
                            timestep_string = literal_string

                        # Evidence without clause
                        else:
                            evidence, name, parameters = line.rstrip().split(' ')
                            if evidence.startswith('evidence'):
                                evidence = True
                            elif evidence.startswith('not_evidence'):
                                evidence = False
                            else:
                                raise Exception('Error in simulations file')
                            if evidence:
                                # If the evidence is about the zone of a car, add it to the timestep_zones_cars dict
                                if name == 'zone_has_car':
                                    timestep_zones_cars.setdefault(time_step, []).append((1,parameters))
                                # If the evidence is about the zone of a pedestrian, add it to the timestep_zones_pedestrians dict
                                if name == 'zone_has_pedestrian':
                                    timestep_zones_pedestrians.setdefault(time_step, []).append((1, parameters))
                            params = self.turn_params_in_list(parameters)
                            timestep_string = self.make_evidence(name, params, evidence)
                        timestep_evidences[time_step] = timestep_evidences.get(time_step, '') + timestep_string + '\n'


        # Add all_cars fact and 'not evidence zone_has_car' clause for not mentioned zones in each timestep
        for time_step, car_list in timestep_zones_cars.iteritems():
            number_cars = len(car_list)
            car_list_string = '['

            clause_string = 'zone(Zone)'
            if number_cars != 0:
                clause_string += ', '


            car_number = 0
            for (_,car) in car_list:
                car_number += 1
                car_list_string += car
                clause_string += 'Zone\=' + car
                if car_number != number_cars:
                    car_list_string += ', '
                    clause_string += ', '
            car_list_string += ']'

            all_cars_fact = self.make_fact('all_cars', [car_list_string])
            not_evidence_cars = self.make_evidence_with_clause('zone_has_car', ['Zone'], False, clause_string)

            timestep_string = all_cars_fact + '\n' + not_evidence_cars

            timestep_evidences[time_step] = timestep_evidences.get(time_step, '')  + timestep_string + '\n'

        # Add 'not evidence zone_has_pedestrian' clause for not mentioned zones in each timestep
        for time_step, pedestrian_list in timestep_zones_pedestrians.iteritems():
            number_pedestrians = len(pedestrian_list)

            clause_string = 'zone(Zone)'
            if number_pedestrians != 0:
                clause_string += ', '

            pedestrian_number = 0
            for (_, pedestrian) in pedestrian_list:
                pedestrian_number += 1
                clause_string += 'Zone\=' + pedestrian
                if pedestrian_number != number_pedestrians:
                    clause_string += ', '

            not_evidence_pedestrians = self.make_evidence_with_clause('zone_has_pedestrian', ['Zone'], False, clause_string)

            timestep_evidences[time_step] = timestep_evidences.get(time_step, '') + not_evidence_pedestrians + '\n'

        # Add evidence that not mentioned pedestrian crossing zones do not contain pedestrian crossings.
        pedestrian_crossing_number = 0
        clause_string = 'zone(Zone)'
        number_pedestrian_zones = len(pedestrian_crossing_zones)
        if number_pedestrian_zones != 0:
            clause_string += ', '
        for pedestrian_crossing in pedestrian_crossing_zones:
            clause_string += 'Zone\=' + pedestrian_crossing
            pedestrian_crossing_number += 1
            if pedestrian_crossing_number != number_pedestrian_zones:
                clause_string += ', '
        not_evidence_pedestrian_crossings = self.make_evidence_with_clause('zone_has_pedestrian', ['Zone'], False, clause_string)
        general_evidences += not_evidence_pedestrian_crossings + '\n'

        return general_evidences, timestep_evidences, timestep_zones_cars, timestep_zones_pedestrians, pedestrian_crossing_zones, route_self