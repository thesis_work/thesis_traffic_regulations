# README #

## Reasoning with traffic regulations for autonomous vehicles ##

* Source code of thesis 'Reasoning with traffic regulations for autonomous vehicles'
* Version 1.0

## Set up ##

* Run in command line: python CrossingProgram.py Simulationsfile IntersectionLayoutfile Graphicsoption  

        -Simulationsfile: contains simulation information  
        -IntersectionLayoutfile: contains the different lanes of the intersection  
        -Graphicsoption: indicating whether graphical representation need to be saved and shown (real time showing does not always work). Values: --graphics or --no-graphics  

* Graphical representations are stored in folder 'figures'
* Results of queries are stored in output.txt  

* Example of run: python CrossingProgram.py Simulations.txt intersection_layout.txt --graphics

## Main requirements ##

* Python 2.7
* ProbLog