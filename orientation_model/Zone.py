"""
Class representing a zone
"""
class Zone:

    def __init__(self, name, position, crossing, sidewalk):
        # The name of the zone
        self.name = name

        # The position of the zone
        self.position = position

        # Boolean defining whether the zone is part of a crossing or not
        self.crossing = crossing

        # Boolean defining whether the zone is a sidewalk or not
        self.sidewalk = sidewalk

        # Boolean defining whether the zone is a pedestrian crossing zone or not
        self.pedestrian_crossing_zone = False




