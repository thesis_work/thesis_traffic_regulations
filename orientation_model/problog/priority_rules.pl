%%% Priority_rules %%%
%% Priority to right intersection %%
priority(Self, CarLane, Intersection) :-
        priority_to_right(Intersection),
        priority_to_right(Self, CarLane).

no_priority(Self, CarLane, Intersection) :-
        priority_to_right(Intersection),
        no_priority_to_right(Self, CarLane).

priority_to_right(Self, CarLane) :-
        destination(Self, ro).

no_priority_to_right(Self, CarLane) :-
        \+ destination(Self, ro),
        CarLane == ri.

no_priority_to_right(Self, CarLane) :-
        destination(Self, lo),
        CarLane == oi.

priority_to_right(Self, CarLane) :-
        destination(Self, oo),
        CarLane \= ri.

priority_to_right(Self, CarLane) :-
        destination(Self, lo),
        CarLane \= ri,
        CarLane \= oi.

% If Car2 is at the intersection, Self has priority over Car2 if Car2 situated on the left part of the intersection.
priority_zone_intersection(Self, Car2, Intersection, Zone) :-
      	priority_to_right(Intersection),
      	orientation(Car2, Orientation),
      	((main_orientation(Orientation),
      	MainOrientation = d);
      	(intermediate_orientation(Orientation),
      	second_intermediate_orientation(Orientation, d))),
      	(zone_in_front_outgoing_lane(do, Car2);
      	zone_in_front_incoming_lane(oi, Car2)).


%% Priority to intersection with lights %%
priority(Self, CarLane, Intersection) :-
        green_light(Light, Intersection, di),
        priority_light(Self, CarLane).

no_priority(Self, CarLane, Intersection) :-
        green_light(Light, Intersection, di),
        no_priority_light(Self, CarLane).

priority_light(Self, CarLane) :-
        \+ destination(Self, lo).

priority_light(Self, CarLane) :-
        destination(Self, lo),
        CarLane \= oi.

no_priority_light(Self, CarLane) :-
        destination(Self, lo),
        CarLane == oi.

% If Car2 is at the intersection, Self has priority when the light is green over Car2 if Car2 situated on the left part of the intersection
priority_zone_intersection(Self, Car2, Intersection, Zone) :-
      	green_light(Light, Intersection, di),
      	orientation(Car2, Orientation),
      	((main_orientation(Orientation),
      	MainOrientation = d);
      	(intermediate_orientation(Orientation),
      	second_intermediate_orientation(Orientation, d))),
      	(zone_in_front_outgoing_lane(do, Car2);
      	zone_in_front_incoming_lane(oi, Car2)).

%% Priority lane %%
priority(Self, CarLane, Intersection) :-
        priority_lane(Intersection, di),
        priority_lane(Self, CarLane).

no_priority(Self, CarLane, Intersection) :-
        priority_lane(Intersection, di),
        no_priority_lane(Self, CarLane).

priority_lane(Self, CarLane) :-
        \+ destination(Self, lo).

priority_lane(Self, CarLane) :-
        destination(Self, lo),
        CarLane \= oi.
no_priority_lane(Self, CarLane) :-
        destination(Self, lo),
        CarLane == oi.

% If Car2 is at the intersection, Self has priority over Car2 if Car2 situated on the left part of the intersection.
priority_zone_intersection(Self, Car2, Intersection, Zone) :-
      	priority_lane(Intersection, di),
      	orientation(Car2, Orientation),
      	((main_orientation(Orientation),
      	MainOrientation = d);
      	(intermediate_orientation(Orientation),
      	second_intermediate_orientation(Orientation, d))),
      	(zone_in_front_outgoing_lane(do, Car2);
      	zone_in_front_incoming_lane(oi, Car2)).

%% Give way%%
priority(Self, CarLane, Intersection) :-
        give_way(Intersection, di),
        priority_give_way(Self, CarLane).

no_priority(Self, CarLane, Intersection) :-
        give_way(Intersection, di),
        no_priority_give_way(Self, CarLane).

no_priority_give_way(Self, CarLane) :-
        CarLane == li.

no_priority_give_way(Self, CarLane) :-
        \+ destination(Self, ro),
        CarLane \= oi.

priority_give_way(Self, CarLane) :-
        CarLane == oi.

priority_give_way(Self, CarLane) :-
        destination(Self, ro),
        CarLane == ri.

%% Stop Sign %%
priority(Self, CarLane, Intersection) :-
        stop(Intersection, di),
        priority_stop(Self, CarLane).

no_priority(Self, CarLane, Intersection) :-
        stop(Intersection, di),
        no_priority_stop(Self, CarLane).

no_priority_stop(Self, CarLane) :-
        CarLane == li.

no_priority_stop(Self, CarLane) :-
        \+ destination(Self, ro),
        CarLane \= oi.

priority_stop(Self, CarLane) :-
        CarLane == oi.

priority_stop(Self, CarLane) :-
        destination(Self, ro),
        CarLane == ri.

%% Combination priority rules %%
1 :: priority_combination(Self, CarLane, Intersection) :-
        priority(Self, CarLane, Intersection),
        \+ no_priority(Self, CarLane, Intersection).

0 :: priority_combination(Self, CarLane, Intersection) :-
        \+ priority(Self, CarLane, Intersection),
        no_priority(Self, CarLane, Intersection).

1 :: priority_combination(Self, CarLane, Intersection) :-
        \+ priority(Self, CarLane, Intersection),
        \+ no_priority(Self, CarLane, Intersection),
        priority_to_right(Self, CarLane).
0 :: priority_combination(Self, CarLane, Intersection) :-
        \+ priority(Self, CarLane, Intersection),
        \+ no_priority(Self, CarLane, Intersection),
        no_priority_to_right(Self, CarLane).


1 :: priority_combination(Self, CarLane, Intersection) :-
        forbidden_direction_lane(Intersection, CarLane).

% Self has priority if car2 is at an incoming lane and needs to give priority.
has_priority(Self, CarZone2, Intersection, Zone) :-
        lane_of_zone(CarZone2, CarLane),
        incoming_lane(CarLane),
        intersection_zone(Zone),
        intersection(Intersection),
        priority_combination(Self, CarLane, Intersection).

% Self has priority if Car2 is situated at the intersection and Car2 can regulatory stop at the intersection to give priority if needed.
has_priority(Self, CarZone2, Intersection, Zone) :-
        intersection_zone(CarZone2),
        priority_zone_intersection(Self, CarZone2, Intersection, Zone).
