%%% Initial probabilities %%%

% Command to be able to overwrite the initial probabilities.
overwrite(_) :- fail.

% Initial probability that there is a car at a zone.
1e-9 :: zone_has_car(Carzone) :-
        zone(Carzone),
        \+ overwrite(zone_has_car(Carzone)).

% Initial probability that there is a pedestrian at a zone.
1e-9 :: zone_has_pedestrian(PedestrianZone) :-
        zone(PedestrianZone),
        \+ overwrite(zone_has_pedestrian(PedestrianZone)).

% Initial probability that there is a pedestrian at a sidewalk zone.
1e-9 :: zone_has_pedestrian(SidewalkZone) :-
        zone(PedestrianZone),
        sidewalk_zone(PedestrianZone, SidewalkZone),
        \+ overwrite(pedestrian(SidewalkZone)).

% Initial probability that a zone is a pedestrian crossing.
1e-9 :: pedestrian_crossing_zone(Zone) :-
        zone(Zone),
        \+ overwrite(pedestrian_crossing_zone(Zone)).

% Initial probabilities for the orientation of a pedestrian.
0.25 :: orientation_pedestrian(Pedestrian, d); 0.25 :: orientation_pedestrian(Pedestrian, l); 0.25 :: orientation_pedestrian(Pedestrian, o); 0.25 :: orientation_pedestrian(Pedestrian, r) :-
        pedestrian(Pedestrian),
        \+ overwrite(pedestrian_orientations(Pedestrian)).

% Initial probability that there is an intersection.
1e-9  :: intersection(intersection) :- \+ overwrite(intersection(intersection)).

% Initial probability that a lane at an intersection has a crossing sign.
1e-9  :: crossing_sign(Intersection, Lane) :-
        intersection(Intersection),
        incoming_lane(Lane),
        \+ overwrite(crossing_sign(Intersection, Lane)).

% Initial probability that a lane at an intersection has lights.
1e-9  :: intersection_with_light(light, Intersection, Lane) :-
        intersection(Intersection),
        incoming_lane(Lane),
        \+ overwrite(intersection_with_light(light, Intersection, Lane)).

% Initial probability for the colors of a light.
0.40 :: green_light(Light, Intersection, Lane); 0.15 :: orange_light(Light, Intersection, Lane); 0.40 :: red_light(Light, Intersection, Lane); 0.05 :: orange_warning_light(Light, Intersection, Lane) :-
        intersection_with_light(Light, Intersection, Lane),
        \+ overwrite(light_colours(Light, Lane, Intersection)).

% Initial probability that a lane at an intersection has a priority sign.
1e-9 :: priority_sign(Intersection, Lane) :-
        intersection(Intersection),
        incoming_lane(Lane),
        \+ overwrite(priority_sign(Intersection, Lane)).

% Initial probability that a lane at an intersection has a yield sign.
1e-9 :: yield_sign(Intersection, Lane) :-
        intersection(Intersection),
        incoming_lane(Lane),
        \+ overwrite(yield_sign(Intersection, Lane)).

% Initial probability that a lane at an intersection has a stop sign
1e-9 :: stop_sign(Intersection, Lane) :-
        intersection(Intersection),
        incoming_lane(Lane),
        \+ overwrite(stop_sign(Intersection, Lane)).

% Initial probability of destination of self.
1/3 :: destination(self, ro); 1/3 :: destination(self, oo); 1/3 :: destination(self, lo).

% Initial probability that a lane has a sign that it is a forbidden direction in the incoming way.
1e-9 :: forbidden_one_way_sign(Intersection, Road) :-
        intersection(Intersection),
        lane(Lane),
        lane_road(Lane, Road),
        \+ overwrite(forbidden_one_way_sign(Road)).

% Initial probability that a lane has a sign that it is a forbidden direction in the outgoing way.
1e-9 :: one_way_sign(Intersection, Road) :-
        intersection(Intersection),
        lane(Lane),
        lane_road(Lane, Road),
        \+ overwrite(one_way_sign(Road)).

% Initial probability that a lane has a sign that it is forbidden in both directions.
1e-9 :: forbidden_two_way_sign(Intersection, Road) :-
        intersection(Intersection),
        lane(Lane),
        lane_road(Lane, Road),
        \+ overwrite(forbidden_two_way_sign(Road)).

% Initial probabilities that an indicator is on/off for a car.
1e-9 :: indicator_left(Car, on); 1e-9 :: indicator_left(Car, off) :-
        car(Car),
        \+ overwrite(indicator_left(Car)).

1e-9 :: indicator_right(Car, on); 1e-9 :: indicator_right(Car, off) :-
        car(Car),
        \+ overwrite(indicator_left(Car)).

% Initial probability that self is located at Zone.
1e-9 :: location(self, Zone) :- zone(Zone).

% Initial probability that a car is stationary.
1e-9 :: stationary_car(Car) :-
        car(Car),
        \+ overwrite(stationary_car(Car)).

% Initial probability that a car is able to stop before it enters a zone.
1e-9 :: able_to_stop_before_zone(Car,Zone) :-
        car(Car),
        zone(Zone),
        \+ overwrite(able_to_stop_before_zone(Car,Zone)).

% Initial probability that a car is stopping.
1e-9 :: is_stopping(Car, Zone) :-
        car(Car),
        zone(Zone),
        \+ overwrite(is_stopping(Car, Zone)).

% Initial probability for orientation of car:
1e-9 :: orientation(Car, Orientation) :-
        car(Car),
        orientation(Orientation).
