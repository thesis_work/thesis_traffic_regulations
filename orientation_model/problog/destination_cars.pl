%%% Destination cars %%%
% If no indicator information is available, chances are equal that car goes to left/straight/right.
NP1 :: destination(Car, D1); NP2 :: destination(Car, D2); NP3 :: destination(Car, D3) :-
      	\+ indicator_information(Car),
        lane_of_zone(Car, Lane),
      	incoming_lane(Lane),
      	lane_road(Lane, Road),
      	left_out(Road, D1),
      	chance_destination(D1, 1/3, 0.01, P1),
      	straight_out(Road, D2),
      	chance_destination(D2, 1/3, 0.01, P2),
      	right_out(Road, D3),
      	chance_destination(D3, 1/3, 0.01, P3),
      	normalize_destination(P1, P2, P3, NP1, NP2, NP3).

% If the left indicator from the Car is on, the chance is high that the Car will go to the left.
NP1 :: destination(Car, D1); NP2 :: destination(Car, D2); NP3 :: destination(Car, D3) :-
      	indicator_left(Car,on),
        lane_of_zone(Car, Lane),
      	incoming_lane(Lane),
      	lane_road(Lane, Road),
      	left_out(Road, D1),
      	chance_destination(D1, 0.95, 0.01, P1),
      	straight_out(Road, D2),
      	chance_destination(D2, 0.025, 0.005, P2),
      	right_out(Road, D3),
      	chance_destination(D3, 0.025, 0.005, P3),
      	normalize_destination(P1, P2, P3, NP1, NP2, NP3).

% If the left indicator from the Car is off, the chance is low that the Car will go to the left.
NP1 :: destination(Car, D1); NP2 :: destination(Car, D2); NP3 :: destination(Car, D3) :-
      	indicator_left(Car,off),
        lane_of_zone(Car, Lane),
      	incoming_lane(Lane),
      	lane_road(Lane, Road),
      	left_out(Road, D1),
      	chance_destination(D1, 0.05, 0.005, P1),
      	straight_out(Road, D2),
      	chance_destination(D2, 0.475, 0.01, P2),
      	right_out(Road, D3),
      	chance_destination(D3, 0.475, 0.01, P3),
      	normalize_destination(P1, P2, P3, NP1, NP2, NP3).

% If the right indicator from the Car is on, the chance is high that the Car will go to the right.
NP1 :: destination(Car, D1); NP2 :: destination(Car, D2); NP3 :: destination(Car, D3) :-
      	indicator_right(Car,on),
        lane_of_zone(Car, Lane),
      	incoming_lane(Lane),
      	lane_road(Lane, Road),
      	left_out(Road, D1),
      	chance_destination(D1, 0.025, 0.005, P1),
      	straight_out(Road, D2),
      	chance_destination(D2, 0.025, 0.005, P2),
      	right_out(Road, D3),
      	chance_destination(D3, 0.95, 0.01, P3),
      	normalize_destination(P1, P2, P3, NP1, NP2, NP3).

% If the right indicator from the Car is off, the chance is low that the Car will go to the right.
NP1 :: destination(Car, D1); NP2 :: destination(Car, D2); NP3 :: destination(Car, D3) :-
      	indicator_right(Car,off),
        lane_of_zone(Car, Lane),
      	incoming_lane(Lane),
      	lane_road(Lane, Road),
      	left_out(Road, D1),
      	chance_destination(D1, 0.475, 0.01, P1),
      	straight_out(Road, D2),
      	chance_destination(D2, 0.475, 0.01, P2),
      	right_out(Road, D3),
      	chance_destination(D3, 0.05, 0.005, P3),
      	normalize_destination(P1, P2, P3, NP1, NP2, NP3).

% If Car is in front of outgoing lane and oriented towards it, the chance is high that it will go to this lane.
NP1 :: destination(Car, D1); NP2 :: destination(Car, D2) :-
      	intersection_zone(Car),
      	orientation(Car, Orientation),
      	main_orientation(Orientation),
      	zone_in_front_outgoing_lane(Lane, Car),
      	lane_road(Lane, Road),
      	Orientation == Road,
      	same_lane_out(Road, D1),
      	chance_destination(D1, 0.95, 0.4, P1),
      	right_out(Road, D2),
      	chance_destination(D2, 0.04, 0.02, P2),
      	normalize_destination(P1, P2, NP1, NP2).

% If Car is in front of an incoming lane and is oriented towards the opposite direction, the chance is high that it will go to the opposite or left lane.
NP1 :: destination(Car, D1); NP2 :: destination(Car, D2); NP3 :: destination(Car, D3) :-
      	intersection_zone(Car),
      	orientation(Car, Orientation),
      	main_orientation(Orientation),
      	zone_in_front_incoming_lane(Lane, Car),
      	lane_road(Lane, Road),
      	opposite_orientation(Road, OppositeOrientation),
      	Orientation == OppositeOrientation,
      	right_out(Road, D1),
      	chance_destination(D1, 0.02, 0.001, P1),
      	straight_out(Road, D2),
      	chance_destination(D2, 0.49, 0.1, P2),
      	left_out(Road, D3),
      	chance_destination(D3, 0.49, 0.1, P3),
      	normalize_destination(P1, P2, P3, NP1, NP2, NP3).

% If Car has an intermediate orientation, the destination is one of the main orientations from which the intermediate orientation consists of.
NP1 :: destination(Car, D1); NP2 :: destination(Car, D2) :-
      	intersection_zone(Car),
      	orientation(Car, Orientation),
      	intermediate_orientation(Orientation),
      	zone_in_front_outgoing_lane(Lane, Car),
      	lane_road(Lane, Road),
      	second_intermediate_orientation(Orientation, SecondOrientation),
      	SecondOrientation == Road,
      	right_out(Road, D1),
      	chance_destination(D1, 0.5, 0.1, P1),
      	same_lane_out(Road, D2),
      	chance_destination(D2, 0.5, 0.1, P2),
      	normalize_destination(P1, P2, NP1, NP2).

% If the left indicator from the Car is on, the chance is high that it will go the outgoing lane situated at its left side.
NP1 :: destination(Car, D1); NP2 :: destination(Car, D2); NP3 :: destination(Car, D3) :-
      	intersection_zone(Car),
      	orientation(Car, Orientation),
      	indicator_left(Car,on),
      	((main_orientation(Orientation),
      	MainOrientation = Orientation);
      	(intermediate_orientation(Orientation),
      	second_intermediate_orientation(Orientation, MainOrientation))),
      	opposite_orientation(MainOrientation, OppOrientation),
      	left_out(OppOrientation, D1),
      	chance_destination(D1, 0.95, 0.01, P1),
      	straight_out(OppOrientation, D2),
      	chance_destination(D2, 0.025, 0.005, P2),
      	right_out(OppOrientation, D3),
      	chance_destination(D3, 0.025, 0.005, P3),
      	normalize_destination(P1, P2, P3, NP1, NP2, NP3).

% If the left indicator from the Car is off, the chance is low that its  destination is the outgoing lane situated at its left side.
NP1 :: destination(Car, D1); NP2 :: destination(Car, D2); NP3 :: destination(Car, D3) :-
      	intersection_zone(Car),
      	orientation(Car, Orientation),
      	indicator_left(Car,off),
      	((main_orientation(Orientation),
      	MainOrientation = Orientation);
      	(intermediate_orientation(Orientation),
      	second_intermediate_orientation(Orientation, MainOrientation))),
      	opposite_orientation(MainOrientation, OppOrientation),
      	left_out(OppOrientation, D1),
      	chance_destination(D1, 0.05, 0.005, P1),
      	straight_out(OppOrientation, D2),
      	chance_destination(D2, 0.475, 0.01, P2),
      	right_out(OppOrientation, D3),
      	chance_destination(D3, 0.475, 0.01, P3),
      	normalize_destination(P1, P2, P3, NP1, NP2, NP3).

% If the right indicator from the Car is on and it is situated at a zone with an outgoing lane at its right side, the chance is high that it will go to this outgoing lane.
NP1 :: destination(Car, D1); NP2 :: destination(Car, D2); NP3 :: destination(Car, D3) :-
      	intersection_zone(Car),
      	orientation(Car, Orientation),
      	indicator_right(Car,on),
      	((main_orientation(Orientation),
      	MainOrientation = Orientation);
      	(intermediate_orientation(Orientation),
      	first_intermediate_orientation(Orientation, MainOrientation))),
      	opposite_orientation(MainOrientation, OppOrientation),
      	right_out(OppOrientation, LaneOut),
      	zone_in_front_outgoing_lane(LaneOut, Car),
      	left_out(OppOrientation, D1),
      	chance_destination(D1, 0.025, 0.005, P1),
      	straight_out(OppOrientation, D2),
      	chance_destination(D2, 0.025, 0.005, P2),
      	right_out(OppOrientation, D3),
      	chance_destination(D3, 0.95, 0.01, P3),
      	normalize_destination(P1, P2, P3, NP1, NP2, NP3).

% If the right indicator from the Car is off and it is situated at a zone with an outgoing lane at its right side, the chance is low that it will go to this outgoing lane.
NP1 :: destination(Car, D1); NP2 :: destination(Car, D2); NP3 :: destination(Car, D3) :-
      	intersection_zone(Car),
      	orientation(Car, Orientation),
      	indicator_right(Car,off),
      	((main_orientation(Orientation),
      	MainOrientation = Orientation);
      	(intermediate_orientation(Orientation),
      	first_intermediate_orientation(Orientation, MainOrientation))),
      	opposite_orientation(MainOrientation, OppOrientation),
      	right_out(OppOrientation, LaneOut),
      	zone_in_front_outgoing_lane(LaneOut, Car),
      	left_out(OppOrientation, D1),
      	chance_destination(D1, 0.475, 0.01, P1),
      	straight_out(OppOrientation, D2),
      	chance_destination(D2, 0.475, 0.01, P2),
      	right_out(OppOrientation, D3),
      	chance_destination(D3, 0.05, 0.005, P3),
      	normalize_destination(P1, P2, P3, NP1, NP2, NP3).

% If the car is at an outgoing lane, its destination is this outgoing lane.
1 :: destination(Car, Lane) :-
        lane_of_zone(Car, Lane),
        outgoing_lane(Lane).

% Normalize chances.
normalize_destination(P1, P2, NP1, NP2) :-
        TotalSum is P1 + P2,
        NP1 is P1/TotalSum,
        NP2 is P2/TotalSum.

% Normalize chances.
normalize_destination(P1, P2, P3, NP1, NP2, NP3) :-
        TotalSum is P1 + P2 + P3,
        NP1 is P1/TotalSum,
        NP2 is P2/TotalSum,
        NP3 is P3/TotalSum.

% Add constraint to normalize chances for all destinations together.
% For each Car, at most one destination is true.
atmost_one(Car) :-
        destination(Car,D1),
        outgoing_lane(D1),
	       \+ (destination(Car, D2), D1 \= D2).

evidence(atmost_one(Car)) :-
        all_cars(Cars),
        member(Car, Cars).

% If Destination is defined and not forbidden, the chance P is equal to the P1.
chance_destination(Destination, P1, P2, P) :-
      	Destination \= notDefined,
      	\+forbidden_direction_lane(_,Destination),
      	P = P1.

% If Destination is forbidden, the chance P is equal to P2.
chance_destination(Car, Destination, P1, P2, P) :-
        forbidden_direction_lane(_,Destination),
        P = P2.

% If Destination is not defined, the chance P is equal to 0.
chance_destination(Destination, P1, P2, P) :-
      	Destination == notDefined,
      	P = 0.
