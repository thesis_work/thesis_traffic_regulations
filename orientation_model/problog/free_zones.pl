%%% Free zone %%%
%% No free zone clauses %%
% The zone is not free if there is a red light at the lane of Self and the zone is the first intersection zone.
no_free_zone(Self, Intersection, Zone) :-
				red_light(Light, Intersection, di),
				zone_in_front_incoming_lane(di, Z),
				Zone = Z.

% The zone is not free if there is a red light at the lane of Self and the zone is a pedestrian crossing zone.
no_free_zone(Self, Intersection, Zone) :-
				red_light(Light, Intersection, di),
				pedestrian_crossing_zone(Zone).

% The zone is not free if there is an orange light at the lane of Self and the zone is an intersection zone.
no_free_zone(Self, Intersection, Zone) :-
				orange_light(Light, Intersection, di),
				zone_in_front_incoming_lane(di, Z),
				Zone = Z.

% The zone is not free if there is an orange light at the lane of Self and the zone is a pedestrian crossing zone.
no_free_zone(Self, Intersection, Zone) :-
				orange_light(Light, Intersection, di),
				pedestrian_crossing_zone(Zone).

%% Free zone clauses %%
% If Car2 uses the zone, the zone is free if Self has priority and Car2 gives priority.
free_zone(Self, Car2, Intersection, Zone) :-
				car(Car2),
				intersection(Intersection),
				zone(Zone),
				destination(Car2, Destination),
				\+ no_free_zone(Self, Intersection, Zone),
				zone_in_route(Zone, Car2, Destination),
				has_priority(Self, Car2, Intersection, Zone),
				intention_give_priority(Car, Zone).

% The zone is free if it is not used by Car2.
free_zone(Self, Car2, Intersection, Zone) :-
				car(Car2),
				intersection(Intersection),
				zone(Zone),
				\+ no_free_zone(Self, Intersection, Zone),
				destination(Car2, Destination),
				\+ zone_in_route(Zone, Car2, Destination).

% The zone is free if self is already at the route of Car2.
free_zone(Self, Car2, Intersection, Zone) :-
				car(Car2),
				intersection(Intersection),
				zone(Zone),
				\+ no_free_zone(Self, Intersection, Zone),
				destination(Car2, Destination),
				location(Self, ZoneSelf),
				zone_in_route(ZoneSelf, Car2, Destination).

 % The zone is free if it is no pedestrian crossing zone.
free_zone(Self, Pedestrian, Intersection, Zone) :-
				pedestrian(Pedestrian),
				Zone \= Pedestrian,
				\+ pedestrian_crossing_zone(Zone).

% The zone is free if it is a pedestrian crossing zone, but the pedestrian has no intention to use it.
free_zone(Self, Pedestrian, Intersection, Zone) :-
				pedestrian(Pedestrian),
				pedestrian_crossing_zone(Zone),
				\+ intention_taking_pedestrian_zone(Pedestrian, Zone).

% The zone is free if the object does not exist.
free_zone(Self, MovingObject, Intersection, Zone) :-
				\+ car(MovingObject),
				\+ pedestrian(MovingObject),
				intersection(Intersection),
				zone(Zone),
				\+ no_free_zone(Self, Intersection, Zone).
