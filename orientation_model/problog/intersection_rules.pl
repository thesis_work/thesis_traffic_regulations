% A car at Lane needs to give way at the Intersection when there is a yield sign without lights.
give_way(Intersection, Lane) :-
        \+ (intersection_with_light(Light, Intersection, Lane), \+ orange_warning_light(Light, Intersection, Lane)),
        yield_sign(Intersection, Lane).

% A car at Lane needs to stop at the Intersection when there is a stop sign without lights.
stop(Intersection, Lane) :-
        \+ (intersection_with_light(Light, Intersection, Lane), \+ orange_warning_light(Light, Intersection, Lane)),
        stop_sign(Intersection, Lane).

% A car at Lane has priority at the Intersection when there is a priority sign without lights.
priority_lane(Intersection, Lane) :-
        \+ (intersection_with_light(Light, Intersection, Lane), \+ orange_warning_light(Light, Intersection, Lane)),
        priority_sign(Intersection, Lane).

% A car needs to give priority at the right when there is a crossing sign (B17).
priority_to_right(Intersection) :- crossing_sign(Intersection, Lane).

% The outgoing direction of the road is forbidden if there is a forbidden sign for one way at the road.
forbidden_direction_lane(Intersection, Lane) :-
        forbidden_one_way_sign(Intersection, Road),
        lane_out(Road, Lane).

% The incoming direction of the road is forbidden if there is a one way sign at the road.
forbidden_direction_lane(Intersection, Lane) :-
        one_way_sign(Intersection, Road),
        lane_in(Road, Lane).

% Both directions of the road are forbidden if there is a forbidden in two ways sign at the road.
forbidden_direction_lane(Intersection, Lane) :-
        forbidden_two_way_sign(Intersection, Road),
        lane_in(Road, Lane).

forbidden_direction_lane(Intersection, Lane) :-
        forbidden_two_way_sign(Intersection, Road),
        lane_out(Road, Lane).
