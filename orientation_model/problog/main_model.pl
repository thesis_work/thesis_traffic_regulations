%%% Main model %%%

:- use_module(library(lists)).

:- use_module('problog/priority_rules.pl').
:- use_module('problog/zones.pl').
:- use_module('problog/initial_probabilities.pl').
:- use_module('problog/cars.pl').
:- use_module('problog/standard_intersection_setting.pl').
:- use_module('problog/intersection_directions.pl').
:- use_module('problog/intersection_rules.pl').
:- use_module('problog/free_zones.pl').
:- use_module('problog/destination_cars.pl').
:- use_module('problog/model_definitions.py').
:- use_module('problog/intention_cars.pl').
:- use_module('problog/pedestrians.pl').
:- use_module('problog/orientations.pl').
