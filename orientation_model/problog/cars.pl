%%% Cars %%%
% Define a car
car(CarZone) :- zone_has_car(CarZone).

indicator_information(Car) :-
        indicator_left(Car,_);
        indicator_right(Car,_).
