%%% Intersection directions %%%
% Define left/right/straight on for the driver side of the intersection.
left(d,l).
right(d,r).
straight(d,o).

% Define left/right/straight on for the right side of the intersection.
left(r,d).
right(r,o).
straight(r,l).

% Define left/right/straight on for the opposite side of the intersection.
left(o,r).
right(o,l).
straight(o,d).

% Define left/right/straight on for the left side of the intersection.
left(l,o).
right(l,d).
straight(l,r).

% Define incoming and outgoing lanes.
left_out(LaneOrientation,LaneOut) :-
        left(LaneOrientation, Left),
        lane_out(Left, Lane),
        valid_lane(Lane, LaneOut).
right_out(LaneOrientation,LaneOut) :-
        right(LaneOrientation, Left),
        lane_out(Left, Lane),
        valid_lane(Lane, LaneOut).
straight_out(LaneOrientation,LaneOut) :-
        straight(LaneOrientation, Left),
        lane_out(Left, Lane),
        valid_lane(Lane, LaneOut).
same_lane_out(LaneOrientation, LaneOut) :-
        lane_out(LaneOrientation, Lane),
        valid_lane(Lane, LaneOut).
left_in(LaneOrientation,LaneOut) :-
        left(LaneOrientation, Left),
        lane_in(Left, Lane),
        valid_lane(Lane, LaneOut).
right_in(LaneOrientation,LaneOut) :-
        right(LaneOrientation, Left),
        lane_in(Left, Lane),
        valid_lane(Lane, LaneOut).
straight_in(LaneOrientation,LaneOut) :-
        straight(LaneOrientation, Left),
        lane_in(Left, Lane),
        valid_lane(Lane, LaneOut).
same_lane_in(LaneOrientation, LaneOut) :-
        lane_in(LaneOrientation, Lane),
        valid_lane(Lane, LaneOut).

% Determine if lane is incoming lane
incoming_lane(Lane) :-
        incoming_lanes(Lanes),
        member(Lane, Lanes).

% Determine if lane is outgoing lane
outgoing_lane(Lane) :-
        outgoing_lanes(Lanes),
        member(Lane, Lanes).

% Define lane
lane(Lane) :-
        incoming_lane(Lane);
        outgoing_lane(Lane).

% Determine if Lane is an exisisting lane, if not return notDefined.
valid_lane(Lane, RealLane) :-
        lane(Lane),
        RealLane = Lane.
valid_lane(Lane, RealLane) :-
        \+ lane(Lane),
        RealLane = notDefined.

% Get opposite orientation
opposite_orientation(Orientation, OppositeOrientation) :-
        (Orientation = d,
        OppositeOrientation = o);
        (Orientation = l,
        OppositeOrientation = r);
        (Orientation = o,
        OppositeOrientation = d);
        (Orientation = r,
        OppositeOrientation = l).
