%%% Orientations %%%
% Define main orientations
main_orientations([d, l, o, r]).

% Define intermediate orientations
intermediate_orientations([dl, lo, or, rd]).

orientation(Orientation) :-
        main_orientation(Orientation);
        intermediate_orientation(Orientation).

main_orientation(Orientation) :-
        main_orientations(Orientations),
        member(Orientation, Orientations).

intermediate_orientation(Orientation) :-
        intermediate_orientations(Orientations),
        member(Orientation, Orientations).
