%%% Standard intersection setting %%%

% Zones followed when going from the starting lane to the destination.
zones_followed(Startlane, Destination, Zones_list) :-
				(Startlane = di,
				((Destination = ro, Zones_list = [zd]);
				(Destination = oo, Zones_list = [zd, zr]);
				(Destination = lo, Zones_list = [zd, zr, zo])));
				(Startlane = ri,
				((Destination = oo, Zones_list = [zr]);
				(Destination = lo, Zones_list = [zr, zo]);
				(Destination = do, Zones_list = [zr, zo, zl])));
				(Startlane = oi,
				((Destination = lo, Zones_list = [zo]);
				(Destination = do, Zones_list = [zo, zl]);
				(Destination = ro, Zones_list = [zo, zl, zd])));
				(Startlane = li,
				((Destination = do, Zones_list = [zl]);
				(Destination = ro, Zones_list = [zl, zd]);
				(Destination = oo, Zones_list = [zl, zd, zr]))).

% Define zones that are in front of a lane.
zone_in_front_incoming_lane(di, zd).
zone_in_front_incoming_lane(ri, zr).
zone_in_front_incoming_lane(oi, zo).
zone_in_front_incoming_lane(li, zl).

% Define zones that are in front of a lane.
zone_in_front_outgoing_lane(do, zl).
zone_in_front_outgoing_lane(ro, zd).
zone_in_front_outgoing_lane(oo, zr).
zone_in_front_outgoing_lane(lo, zo).

% Define zones
zone(zd).
zone(zr).
zone(zo).
zone(zl).
zone(zlo1).
zone(zlo2).
zone(zlo3).
zone(zli1).
zone(zli2).
zone(zli3).

zone(zdo1).
zone(zdo2).
zone(zdo3).
zone(zdi1).
zone(zdi2).
zone(zdi3).

zone(zro1).
zone(zro2).
zone(zro3).
zone(zri1).
zone(zri2).
zone(zri3).

zone(zoo1).
zone(zoo2).
zone(zoo3).
zone(zoi1).
zone(zoi2).
zone(zoi3).

% Define zones of lanes
lane_zones(li, [zli1, zli2, zli3]).
lane_zones(lo, [zlo1, zlo2, zlo3]).
lane_zones(di, [zdi1, zdi2, zdi3]).
lane_zones(do, [zdo1, zdo2, zdo3]).
lane_zones(ri, [zri1, zri2, zri3]).
lane_zones(ro, [zro1, zro2, zro3]).
lane_zones(oi, [zoi1, zoi2, zoi3]).
lane_zones(oo, [zoo1, zoo2, zoo3]).

% Define intersection zones
intersection_zones([zd, zr, zo, zl]).
