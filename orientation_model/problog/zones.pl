% Get the lane where the zone is part of.
lane_of_zone(Zone, Lane) :-
        lane_zones(Lane, Zones),
        member(Zone, Zones).

% Check whether zone is intersection zone.
intersection_zone(Zone) :-
        zone(Zone),
        intersection_zones(Zones),
        member(Zone, Zones).

% Check whether the Zone is in the route of the Car to the Destination.
zone_in_route(Zone, Car, Destination) :-
      	zones_to_follow(Car, Destination, ZonesToFollow),
      	member(Zone,ZonesToFollow).

% The zone is in the route when the car is standing at it.
zone_in_route(Zone, Car, Destination) :-
        Zone == Car.

%% The zones that need to be followed when the car is positioned at Zone with StartLane and Destination.
% The Zone is at the intersection.
zones_to_follow(CurrentZone, Destination, ZonesToFollow) :-
      	intersection_zone(CurrentZone),
      	findall(Zones, remaining_route(CurrentZone, Destination, Zones), DuplicateZonesToFollow),
      	DuplicateZonesToFollow = [ZonesToFollow|Rest].

% The Zone is at an incoming lane.
zones_to_follow(CurrentZone, Destination, ZonesToFollow) :-
      	lane_of_zone(CurrentZone, Lane),
      	incoming_lane(Lane),
      	zones_followed(Lane, Destination, ZonesToFollow).

% The Zone is at an outgoing lane.
zones_to_follow(Zone, Destination, ZonesToFollow) :-
      	lane_of_zone(Zone, Lane),
      	outgoing_lane(Lane),
      	ZonesToFollow = [].

% Get remaning route starting from current zone.
remaining_route(CurrentZone, Destination, RemainingZones) :-
      	zones_followed(_, Destination, ZonesList),
      	member(CurrentZone, ZonesList),
      	remaining_zones(CurrentZone, ZonesList, RemainingZones).

remaining_zones(Zone, [FirstZone|Rest], Rest) :-
	     Zone == FirstZone.

remaining_zones(Zone, [FirstZone|Rest], RemainingZones) :-
      	Zone \= FirstZone,
      	remaining_zones(Zone, Rest, RemainingZones).
