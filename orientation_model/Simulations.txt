----BeginSimulation
% The route of self
% Format: destination_lane route_of_zones
--Self route
oo zdi2,zdi1,zd,zr,zoo1,zoo2
% The general information holds for the whole run time of the program
% Format: evidence/not_evidence name parameters
% Evidence with a clause is also possible
% Format: clause_evidence/clause_not_evidence name parameters clause
% Probabilities are possible
% Format: prob P name parameters
% Own defined probability composition that need overwrite (i.e. for chances that need to be normalized )
% Format overwrite: Overwrite name parameters
% Format own probabilities: composition_prob rule
---GeneralInformation
evidence intersection intersection
not_evidence crossing_sign intersection,di
not_evidence intersection_with_light light,intersection,di
evidence yield_sign intersection,di
not_evidence stop_sign intersection,di
not_evidence priority_sign intersection,di
not_evidence forbidden_one_way_sign intersection,_
not_evidence one_way_sign intersection,_
not_evidence forbidden_two_way_sign intersection,_
evidence pedestrian_crossing_zone zri1
evidence pedestrian_crossing_zone zro1
% The information that holds for one timestep
% Format: evidence/not_evidence name parameters
% Evidence with a clause is also possible
% Format: clause_evidence/clause_not_evidence name parameters clause
% Probabilities are possible
% Format: prob P name parameters
% Own defined probability composition that need overwrite (i.e. for chances that need to be normalized )
% Format overwrite: Overwrite name parameters
% Format own probabilities: composition_prob rule
---DiscreteInformation
**t=0
evidence zone_has_car zli1
evidence orientation zli1,r
evidence indicator_right zli1,on
evidence indicator_left zli1,off
**t=1
evidence zone_has_car zl
evidence orientation zl,rd
evidence indicator_right zl,on
evidence indicator_left zl,off
**t=2
evidence zone_has_car zdo1
evidence orientation zdo1,d
evidence indicator_right zdo1,off
evidence indicator_left zdo1,off
**t=3
evidence zone_has_car zdo2
evidence orientation zdo2,d
evidence indicator_right zdo2,off
evidence indicator_left zdo2,off
---EndSimulation
